#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h" /* auto generated*/
#include "library.h"/* header file that contains the structure of functions*/

Library *make_library(unsigned id) 
{

   Library *lib;

   /* creat a memory area using malloc which would later be used for storage andretrieval of data*/
   if ((lib = (Library *)malloc(sizeof(Library))) == NULL) 
     {
       fprintf(stderr, "Failed to allocate Library structure!\n");
       exit(EXIT_FAILURE);
     }

  /*  if (( lib->openingTime=(Time*)malloc(sizeof(Library))) == NULL){ // malloc creates a space in memory 
    fprintf(stderr," Failed to allocate movie structure !\n");
    exit(EXIT_FAILURE);
  } */
   /* assign values to variables in Library*/
   lib->active = 0;
   lib->id = id;
   lib->name = NULL;
   lib->address = NULL;
   /*  lib->openingTime->hr = 7;
       lib->openingTime->min = 30;
       lib->openingTime->sec = 0;
   */ 

   return lib;
   //   }
   /* free memory allocated to dfferent variables*/
   void free_library(Library *lib) 
   {
     free(lib->name);
     free(lib->address);
     /* free(lib->openingTime);*/
     free(lib);
   }

   /* assign appropriate values to different variables*/ 
   void make_library_active(Library *lib) 
   {
     lib->active = 1;
   }

   void set_library_name(Library *lib, char *name)
   {
     lib->name = strdup(name);  /* strdup() function creates a copy of the location where the data is stored.*/
   }

   void set_library_address(Library *lib, char *address)
   {
     lib->address = strdup(address);
   }

   int is_library_active(Library *lib) 
   {
     return lib->active;
   }

   /* serialize data*/
   int serialize_library(char *buffer, Library *lib)
   {
     size_t offset = 0;

     memcpy(buffer, &lib->id, sizeof(lib->id));
     offset = sizeof(lib->id);
     memcpy(buffer+offset, &lib->active, sizeof(lib->active));
     offset = offset + sizeof(lib->active);
     memcpy(buffer+offset, lib->name, strlen(lib->name)+1);
     offset = offset + strlen(lib->name)+1;
     memcpy(buffer+offset, lib->address, strlen(lib->address)+1);
     offset = offset + strlen(lib->address)+1;

  /*
  memcpy(buffer+offset, &lib->openingTime->hr,sizeof(lib->openingTime->hr));
  offset = offset +  sizeof(lib->openingTime->hr)+1;
  memcpy(buffer+offset, &lib->openingTime->min,sizeof(lib->openingTime->min));
  offset = offset +  sizeof(lib->openingTime->min)+1;
  memcpy(buffer+offset, &lib->openingTime->sec,sizeof(lib->openingTime->sec));
  offset = offset + sizeof(lib->openingTime->sec)+1;
*/

     return offset;
   }
   /* Deserialize data*/
   int deserialize_library(char *buffer, Library *lib)
   {
     size_t offset = 0;

     memcpy(&lib->id, buffer, sizeof(lib->id));
     offset = sizeof(lib->id);
     memcpy(&lib->active, buffer+offset, sizeof(lib->active));
     offset = offset + sizeof(lib->active);
     memcpy(lib->name, buffer+offset, strlen(buffer+offset)+1);
     offset = offset + strlen(buffer+offset)+1;  
     memcpy(lib->address, buffer+offset,sizeof(lib->address));
     offset = offset + strlen(buffer+offset)+1;
    /*
    memcpy(&lib->openingTime->hr, buffer+offset, sizeof(lib->openingTime->hr));
    offset = offset + sizeof(lib->openingTime->hr);
    memcpy(&lib->openingTime->min, buffer+offset, sizeof(lib->openingTime->min));
    offset = offset +   sizeof(lib->openingTime->min);
    memcpy(&lib->openingTime->sec, buffer+offset, sizeof(lib->openingTime->sec));
    offset = offset +   sizeof(lib->openingTime->sec);
    */					      

     return offset;
   }

   /* print data*/
   void print_library(Library *lib)
   {
     printf("Library id:%u\n", lib->id);
     printf("Library name:%s\n", lib->name);
     printf("%s is located at:   %s\n", lib->name, lib->address);
     /* printf("Library Opening Time:   %d-%d-%d-%d\n", lib->openingTime->hr, lib->openingTime->min, lib->openingTime->sec);*/
   }

   /* alloc_blank is a structure where we store data we serialize and which can be retrieved during deserialization*/
   Library *alloc_blank_library() 
   {
     Library *lib;
     if ((lib = (Library *)malloc(sizeof(Library))) == NULL)
       {
       fprintf(stderr, "Failed to allocate Library structure!\n");
       exit(EXIT_FAILURE);
       }
     lib->active = 0;
     lib->id = 0;
     if ((lib->name = malloc(MAX_NAME)) == NULL)
       {
	 fprintf(stderr, "Failed to allocate name!\n");
	 exit(EXIT_FAILURE);
       }

     if ((lib->address = malloc(MAX_NAME)) == NULL)
       {
	 fprintf(stderr, "Failed to allocate address!\n");
	 exit(EXIT_FAILURE);
       }
  /* if ((lib->openingTime->hr = malloc(MAX_NAME)) == NULL) {
    fprintf(stderr, "Failed to allocate opening hours!\n");
    exit(EXIT_FAILURE);
  }

if ((lib->openingTime->min = malloc(MAX_NAME)) == NULL) {
    fprintf(stderr, "Failed to allocate opening minutes!\n");
    exit(EXIT_FAILURE);
  }
 
if ((lib->openingTime->sec = malloc(MAX_NAME)) == NULL) {
    fprintf(stderr, "Failed to allocate opening seconds!\n");
    exit(EXIT_FAILURE);
    }*/

     return lib;
   }
