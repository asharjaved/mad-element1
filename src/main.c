#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#include <stdlib.h>
#include <stdio.h>

#include <errno.h>     /*  This will allown us to capture error, also the variable 'errno' used in main is defined in this header file*/
#define MAXCHARS 256  /* Max length of array that we will define later in the programme*/
#include "library.h"  /* This header file containes the structure defination of all the functions*/
#include "config.h" /* auto generated*/

int main()
{
   Library *lib1;
   int      sock;                                                 
   struct    sockaddr_in servaddr;  
   char      buffer[MAX_BUFFER];                    
   ssize_t bytes_encoded, bytes_sent;
   char *end = NULL;
   unsigned id = 0;
   errno =0;       /*n defined in header file 'errno.h'*/

   if ( (sock = socket(AF_INET, SOCK_STREAM, 0)) < 0 )
     {
       fprintf(stderr, "Error creating socket.\n");
       exit(EXIT_FAILURE);
   }
   
   memset(&servaddr, 0, sizeof(servaddr));
   servaddr.sin_family      = AF_INET;
   servaddr.sin_addr.s_addr = inet_addr(HOST_IP);
   servaddr.sin_port        = htons(HOST_PORT);

   if (connect(sock, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0)
     {
       fprintf(stderr, "Error calling connect()\n");
       exit(EXIT_FAILURE);
   }

   /* Library ID Input*/
   printf("Enter Library ID:  ");

   if (fgets(buffer, MAXCHARS, stdin) == NULL)/*fgets is used to get input from user, MAXCHAR is constant for length and stdin means we are getting input from keyboard*/
     {
       fprintf(stderr, "failed to read string\n");
       return (EXIT_FAILURE);
     } 

   /* remove the new line character*/
   strtok(buffer, "\n");
   /* convert the string to integer*/
   id = strtoul(buffer, &end, 10);
   if(errno != 0)
     {
       fprintf(stderr, "conversion error, %s\n", strerror(errno));
       exit (EXIT_FAILURE);
     }
   else if (*end)
     {
       printf("Warning: converted partially: %i, non-convertible part: %s\n", id, end);
     }

   /* set the value of library id and activate it*/
   lib1 = make_library(id);
   make_library_active(lib1);
   
   /*Library Name Input*/
   
   printf("Enter Library Name:  ");
   if (fgets(buffer, MAXCHARS, stdin) == NULL)
     {
       fprintf(stderr, "failed to read string\n");
       return (EXIT_FAILURE);
     }
   
   /*remove the null character*/
   strtok(buffer, "\n");
   /* set value of name in memory*/
   set_library_name(lib1, buffer);

   printf("Enter Library Locationc:  ");
   if (fgets(buffer, MAXCHARS, stdin) == NULL){

   fprintf(stderr, "failed to read string\n");
   return (EXIT_FAILURE);
   }
   /* remove the null character*/
   strtok(buffer, "\n");
   /* set the address value in memory*/
   set_library_address(lib1, buffer);


   /* printf("opening time of library is:  %d %d %d \n", lib1->openingTime->hr, lib1->openingTime->min,*/
   /* lib1->openingTime->sec);*/


   bytes_encoded = serialize_library(buffer, lib1);
   printf("bytes_encoded = %d\n", (int)bytes_encoded);
   bytes_sent = send(sock, buffer, bytes_encoded, 0);

   if (bytes_sent != bytes_encoded) 
     {
        fprintf(stderr, "Error calling send()\n");
	exit(EXIT_FAILURE);
     }

   if ( close(sock) < 0 )
     {
       fprintf(stderr, "Error calling close()\n");
       exit(EXIT_FAILURE);
     }
   /*free memory*/
    free_library(lib1);
    return EXIT_SUCCESS;

}
