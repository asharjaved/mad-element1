
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#include <stdlib.h>
#include <stdio.h>

#include "library.h"
#include "config.h"

int main(int argc, char *argv[]) {
  int       list_s;                
  int       conn_s;                                 
  struct    sockaddr_in servaddr;  
  char      buffer[MAX_BUFFER];                    
  ssize_t bytes;

  Library *lib1;
  
  if ( (list_s = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) {
    fprintf(stderr, "Error creating listening socket.\n");
    exit(EXIT_FAILURE);
  }
  
  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family      = AF_INET;
  servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servaddr.sin_port        = htons(HOST_PORT);
  
  
  if ( bind(list_s, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0 ) {
    fprintf(stderr, "Error calling bind()\n");
    exit(EXIT_FAILURE);
  }
  
  if ( listen(list_s, 5) < 0 ) {
    fprintf(stderr, "Error calling listen()\n");
    exit(EXIT_FAILURE);
  }
  
  
  if ( (conn_s = accept(list_s, NULL, NULL) ) < 0 ) {
    fprintf(stderr, "Error calling accept()\n");
    exit(EXIT_FAILURE);
  }

  lib1 = alloc_blank_library();
  
  bytes = read(conn_s, buffer, MAX_BUFFER);
  printf("bytes_received=%d\n", (int)bytes);

  deserialize_library(buffer, lib1);
  
  if ( close(conn_s) < 0 ) {
    fprintf(stderr, "Error calling close()\n");
    exit(EXIT_FAILURE);
  }

  print_library(lib1);
  
  free_library(lib1);

  return (EXIT_SUCCESS);
}


