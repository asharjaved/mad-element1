/* define a structure for opening time of library*/
typedef struct{
  int hr;
  int min;
  int sec;
}Time;

/* Define structure Library that also has a pointer to Time structure defined above*/ 
typedef struct {
  unsigned id;
  int active;
  char *name;
  char *address;
  /*  Time *openingTime; */
} Library;

/* Define the structures of all the functions*/ 

Library *make_library(unsigned id);
void free_library(Library *lib);
void make_library_active(Library *lib);
void set_library_name(Library *lib, char *name);
void set_library_address(Library *lib, char *address);

int is_library_active(Library *lib);
int serialize_library(char *buffer, Library *lib);
int deserialize_library(char *buffer, Library *lib);
void print_library(Library *lib);
Library *alloc_blank_library();

